use serde::ser::SerializeStruct;
use serde::{Serialize, Serializer};
#[derive(Debug)]
struct Example<'a> {
    foo: u32,
    bar: &'a str,
    baz: Nested,
}

/// Implementation of Serialize for Example
///
/// Serializer: Implementated by the data format; Translates from the serde
///             data model to the format (https://serde.rs/data-model.html)
///             https://docs.rs/serde/latest/serde/trait.Serializer.html
///
/// Serialize: Tells the serializer how to serialize following the serde data
///            model
///            https://docs.rs/serde/latest/serde/trait.Serialize.html
impl Serialize for Example<'_> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut st = serializer.serialize_struct("Example", 3)?;
        st.serialize_field("foo", &self.foo)?;
        st.serialize_field("bar", &self.bar)?;
        st.serialize_field("baz", &self.baz)?;
        st.end()
    }
}

#[derive(Debug)]
struct Nested {
    values: Vec<String>,
    optional1: Option<u32>,
    optional2: Option<u32>,
}

impl Serialize for Nested {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut st = serializer.serialize_struct("Nested", 3)?;

        st.serialize_field("Badgers", &self.values)?;

        match self.optional1 {
            Some(ref o) => st.serialize_field("Optional1", o)?,
            None => st.skip_field("Optional1")?,
        }

        match self.optional2 {
            Some(ref o) => st.serialize_field("TheOtherOptional", o)?,
            None => st.skip_field("TheOtherOptional")?,
        }

        st.end()
    }
}

fn main() {
    let e = Example {
        foo: 42,
        bar: "mushroom",
        baz: Nested {
            values: vec!["badger".to_string(), "badger".to_string()],
            optional1: None,
            optional2: Some(47),
        },
    };

    println!("==== Serialize ====");
    println!("{}\n", serde_json::to_string_pretty(&e).unwrap());
}
