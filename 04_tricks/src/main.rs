#![allow(clippy::blacklisted_name)]
use serde::de::{Deserializer, Error, Unexpected};
use serde::Deserialize;
use serde_json::Value as JsonValue;

/// Deserialize both numbers and strings (if possible) into a u32
/// Handy for dealing with API's that aren't very strict about their types
fn into_u32<'de, D>(deserializer: D) -> Result<u32, D::Error>
where
    D: Deserializer<'de>,
{
    #[derive(Deserialize)]
    #[serde(untagged)]
    enum IorS<'a> {
        I(u32),
        S(&'a str),
    }

    let iors = IorS::deserialize(deserializer)?;
    match iors {
        IorS::I(v) => Ok(v),
        IorS::S(s) => s
            .parse()
            .map_err(|_| D::Error::invalid_type(Unexpected::Str(&s), &"a u32")),
    }
}

#[derive(Debug, Deserialize, Eq, PartialEq)]
struct Bla {
    #[serde(deserialize_with = "into_u32")]
    foo: u32,
    // Collect other values that were not picked up
    #[serde(flatten)]
    unparsed: JsonValue,
}

fn main() {
    let i = r#"{ "foo": 42 }"#;
    let s = r#"{
        "foo": "42",
        "bar": "snake",
        "baz": {
            "bar": [1, 2, 3, "mushroom"],
            "baz": "Hi"
        }
    }"#;

    let bi: Bla = serde_json::from_str(i).unwrap();
    let bs: Bla = serde_json::from_str(s).unwrap();

    println!("{:#?}", bs);
    assert_eq!(bi.foo, bs.foo);
}
