//! Basic example using the serde crate;
//! For more some great documentation on serde
//! see https://serde.rs
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
struct Example<'a> {
    foo: u32,
    bar: &'a str,
    baz: Nested,
}

#[derive(Debug, Serialize, Deserialize)]
struct Nested {
    values: Vec<String>,
    optional1: Option<u32>,
    optional2: Option<u32>,
}

fn main() {
    let e = Example {
        foo: 42,
        bar: "mushroom",
        baz: Nested {
            values: vec!["badger".to_string(), "badger".to_string()],
            optional1: None,
            optional2: Some(47),
        },
    };

    println!("==== Serialize ====");
    println!("{}\n", serde_json::to_string_pretty(&e).unwrap());

    println!("==== DeSerialize ====");
    let toml = r#"
      foo = 42
      bar = "mushroom"

      [baz]
      values = [ "a", "b" ] 
    "#;

    println!(
        "{:#?}",
        toml::from_str::<Example>(toml).expect("deserialization failed")
    );
}
