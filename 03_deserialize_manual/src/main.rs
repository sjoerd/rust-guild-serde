#![allow(clippy::blacklisted_name)]
use serde::de::{Deserializer, Error, IgnoredAny, MapAccess, Visitor};
use serde::Deserialize;
use std::fmt;
use std::marker::PhantomData;

#[derive(Debug)]
struct Example<'a> {
    foo: u32,
    bar: &'a str,
    baz: Nested,
}

///  Keep in mind; Serde is meant to support both self-describing formats
///  and formats that are not!
///  Deserializer: Does the deserialization, implemented by the data format
///               https://docs.rs/serde/latest/serde/trait.Deserializer.html
///
///  Deserialize: Tells the deserializer *what* it is expected to
///               deserialize (u8? structs? other?)
///               https://docs.rs/serde/latest/serde/trait.Deserialize.html
///  Visitor: Receives the deserialized data
///               https://docs.rs/serde/latest/serde/de/trait.Visitor.html
///
impl<'de: 'a, 'a> Deserialize<'de> for Example<'a> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct V<'de> {
            marker: PhantomData<Example<'de>>,
        };

        impl<'de> Visitor<'de> for V<'de> {
            type Value = Example<'de>;

            fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
                write!(f, "a map")
            }

            fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error>
            where
                A: MapAccess<'de>,
            {
                let mut foo: Option<u32> = None;
                let mut bar: Option<&'de str> = None;
                let mut baz: Option<Nested> = None;

                while let Some(key) = map.next_key()? {
                    match key {
                        "foo" => {
                            foo = match foo {
                                Some(_) => return Err(A::Error::duplicate_field("foo")),
                                None => Some(map.next_value()?),
                            };
                        }
                        "bar" => {
                            bar = match bar {
                                Some(_) => return Err(A::Error::duplicate_field("bar")),
                                None => Some(map.next_value()?),
                            };
                        }
                        "baz" => {
                            baz = match baz {
                                Some(_) => return Err(A::Error::duplicate_field("bar")),
                                None => Some(map.next_value()?),
                            };
                        }
                        _ => {
                            map.next_value::<IgnoredAny>()?;
                        }
                    }
                }

                let foo = foo.ok_or_else(|| A::Error::missing_field("foo"))?;
                let bar = bar.ok_or_else(|| A::Error::missing_field("bar"))?;
                let baz = baz.ok_or_else(|| A::Error::missing_field("baz"))?;

                Ok(Example { foo, bar, baz })
            }
        }

        let visitor = V {
            marker: PhantomData,
        };

        let fields: &'static [&'static str; 3] = &["foo", "bar", "baz"];
        deserializer.deserialize_struct("Example", fields, visitor)
    }
}

#[derive(Debug, Deserialize)]
struct Nested {
    values: Vec<String>,
    optional1: Option<u32>,
    optional2: Option<u32>,
}

fn main() {
    println!("==== DeSerialize ====");
    let toml = r#"
      foo = 42
      bar = "mushroom"

      [baz]
      values = [ "a", "b" ] 
      optional1 = 5
    "#
    .to_string();

    let example: Example = toml::from_str(&toml).expect("failed to deserialize");
    println!("{:#?}", example);
}
