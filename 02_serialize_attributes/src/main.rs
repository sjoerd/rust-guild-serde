//! Show the usage of customization via attributes.
//! more documentation: https://serde.rs/attributes.html
use serde::{Serialize, Serializer};

#[derive(Debug, Serialize)]
#[serde(rename_all = "SCREAMING-KEBAB-CASE")]
struct Example<'a> {
    foo_bar_baz: u32,
    bar_baz: &'a str,
    baz: Nested,
}
#[derive(Debug, Serialize)]
struct Nested {
    #[serde(rename = "Badgers")]
    values: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    optional1: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none", rename = "TheOtherOptional")]
    optional2: Option<u32>,
    #[serde(serialize_with = "duplicate")]
    half: u32,
}

fn duplicate<S>(value: &u32, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_u64(*value as u64 * 2)
}

fn main() {
    let e = Example {
        foo_bar_baz: 42,
        bar_baz: "mushroom",
        baz: Nested {
            values: vec!["badger".to_string(), "badger".to_string()],
            optional1: None,
            optional2: Some(47),
            half: 21,
        },
    };

    println!("==== Serialize ====");
    println!("{}\n", serde_json::to_string_pretty(&e).unwrap());
}
